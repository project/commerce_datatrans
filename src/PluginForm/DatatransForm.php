<?php

namespace Drupal\commerce_datatrans\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a checkout form for the Datatrans gateway.
 */
class DatatransForm extends PaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_datatrans\Plugin\Commerce\PaymentGateway\Datatrans $gateway */
    $gateway = $payment->getPaymentGateway()->getPlugin();
    /** @var \Drupal\Core\Language\LanguageInterface $language */
    $language = \Drupal::languageManager()->getCurrentLanguage();

    $response = $gateway->initializePayment($payment, [
      'redirect' => [
        'successUrl' => $form['#return_url'],
        'cancelUrl' => $form['#cancel_url'],
        'errorUrl' => $form['#return_url'],
      ],
      'language' => $language->getId(),
    ]);

    return $this->buildRedirectForm($form, $form_state, $response->getLocation(), []);
  }

}
