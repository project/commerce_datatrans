<?php

namespace Drupal\commerce_datatrans\Controller;

use Drupal\commerce_datatrans\DatatransHelper;
use Drupal\commerce_datatrans\Plugin\Commerce\PaymentGateway\Datatrans;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PaymentOrderUpdaterInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides the endpoint for payment notifications.
 */
class PaymentNotificationController extends ControllerBase {

  /**
   * The payment order updater.
   *
   * @var \Drupal\commerce_payment\PaymentOrderUpdaterInterface
   */
  protected $paymentOrderUpdater;

  public function __construct(PaymentOrderUpdaterInterface $payment_order_updater) {
    $this->paymentOrderUpdater = $payment_order_updater;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('commerce_payment.order_updater'));
  }

  /**
   * Provides the datatrans webhook.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function notifyPage(Request $request) {

    $content = (string) $request->getContent();
    if (!$content) {
      throw new NotFoundHttpException();
    }

    $transaction_data = Json::decode($content);
    if (!$transaction_data || empty($transaction_data['refno'])) {
      throw new NotFoundHttpException();
    }

    $order_id = $transaction_data['refno'];
    $order = $this->entityTypeManager()->getStorage('commerce_order')->load($order_id);
    if (!$order instanceof OrderInterface) {
      throw new NotFoundHttpException();
    }

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

    // If there is an existing payment for this transaction, use its payment
    // gateway.
    $payments = $payment_storage->loadByProperties([
      'remote_id' => $transaction_data['transactionId'],
      'order_id' => $order->id(),
    ]);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $payments ? reset($payments) : NULL;
    if ($payment) {
      $gateway = $payment->getPaymentGateway();
    }
    else {
      $gateway = $order->get('payment_gateway')->entity;
    }

    if (!$gateway || !$gateway->getPlugin() instanceof Datatrans) {
      throw new NotFoundHttpException();
    }

    /** @var \Drupal\commerce_datatrans\Plugin\Commerce\PaymentGateway\Datatrans $gateway_plugin */
    $gateway_plugin = $gateway->getPlugin();

    $sign2_hmac_key = $gateway_plugin->getConfiguration()['hmac_key_2'];
    if (!$sign2_hmac_key) {
      throw new AccessDeniedHttpException('The webhook is only supported when sign2 is configured.');
    }

    // If the sign2 HMAC key is configured, use it to verify the data.
    $is_valid = FALSE;
    if ($signature_header = $request->headers->get('Datatrans-Signature')) {
      if (preg_match('/t=(\d+),s0=([a-z0-9]+)/', $signature_header, $match)) {
        $timestamp = $match[1];
        $sign = $match[2];

        $is_valid = $sign == DatatransHelper::generateSign($sign2_hmac_key, $timestamp, $content);
      }
    }
    else {
      $this->getLogger('commerce_datatrans')->warning('Received datatrans webhook request without Datatrans-Signature header.');
      throw new AccessDeniedHttpException();
    }

    if (!$is_valid) {
      $this->getLogger('commerce_datatrans')->warning('Received datatrans webhook request with invalid signature.');
      throw new AccessDeniedHttpException();
    }

    // Skip processing if this is not a known state, for example declined.
    // @todo Avoid duplicating this logic? Should decline instead create a
    //   payment with a failed state so onReturn() does not need to repeat the
    //   check?
    if (!\in_array($transaction_data['status'], ['settled', 'transmitted', 'authorized'])) {
      return new Response('', 201);
    }

    $payment = $gateway_plugin->processPayment($transaction_data, $order);

    if ($payment) {

      // Immediately trigger the payment updater, as doing that during
      // the kernel destruct event might result in race conditions with
      // the user returning from Datatrans.
      $this->paymentOrderUpdater->updateOrders();

      $this->getLogger('commerce_datatrans')->notice('Processed datatrans webhook for order @order_id, payment created.', [
        '@order_id' => $order->id(),
      ]);
    }

    return new Response('', 201);
  }

}
