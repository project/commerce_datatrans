<?php

namespace Drupal\commerce_datatrans\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_datatrans\PaymentInitializeResponse;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the base class of the Datatrans payment gateway.
 */
class DatatransBase extends PaymentGatewayBase implements SupportsRefundsInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $gateway = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $gateway->httpClient = $container->get('http_client');
    $gateway->moduleHandler = $container->get('module_handler');
    return $gateway;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'merchant_id' => '',
        'auto_settle' => TRUE,
        'use_alias' => FALSE,
        'hmac_key_2' => '',
        'password' => '',
        'no_initial_amount' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => t('Merchant-ID'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];

    $form['password'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t('The password can be set under Server-to-Server UPP security settings'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['password'],
    );

    $form['hmac_key_2'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Webhook Sign Key (sign2)'),
      '#description' => $this->t('Required when using webhooks. Note that the webhook (URL POST) must be configured in the Datatrans Backend and only a single, static URL is supported per merchant. Webhook URL: @webhook_url.', [
        '@webhook_url' => Url::fromRoute('commerce_datatrans.notify')->setAbsolute()->toString(),
      ]),
      '#default_value' => $this->configuration['hmac_key_2'],
    );

    $form['auto_settle'] = [
      '#type' => 'checkbox',
      '#title' => t('Automatically settle the payment'),
      '#default_value' => $this->configuration['auto_settle'],
    ];

    $form['use_alias'] = [
      '#type' => 'checkbox',
      '#title' => 'Use Alias',
      '#default_value' => $this->configuration['use_alias'],
      '#description' => t('Enable this option to always request an alias from datatrans. This is used for recurring payments and should be disabled if not necessary. If the response does not provide an alias, the payment will not be settled (or refunded, in case it was settled immediately) and the payment needs to be repeated.'),
    ];

    $form['no_initial_amount'] = [
      '#type' => 'checkbox',
      '#title' => 'Initiate the initial payment without amount',
      '#default_value' => $this->configuration['no_initial_amount'],
      '#description' => t('THis is is necessary to use aliases with payment methods like Twint and Paypal. Aliases must be enabled and it might not work with all payment methods.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['auto_settle'] = $values['auto_settle'];
      $this->configuration['use_alias'] = $values['use_alias'];
      $this->configuration['hmac_key_2'] = $values['hmac_key_2'];
      $this->configuration['password'] = $values['password'];
      $this->configuration['no_initial_amount'] = $values['no_initial_amount'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    return new Response('The default notification callback is not supported.', 400);
  }

  /**
   * Process the payment.
   *
   * @param array $transaction_data
   *   Transaction status data.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|bool
   *   The payment entity or boolean false if the payment with
   *   this authorisation code was already processed.
   */
  public function processPayment(array $transaction_data, OrderInterface $order) {
    $payment = $this->getPaymentForTransactionId($transaction_data['transactionId']);
    if ($payment && $payment->getState()->value != 'pending') {
      return FALSE;
    }

    if (!$payment) {
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      // Create a payment if there is none yet.
      // @todo: Support non-complete payment amount for new payment?
      $payment = $payment_storage->create([
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $this->parentEntity->id(),
        'order_id' => $order->id(),
        'remote_id' => $transaction_data['transactionId'],
      ]);
    }

    // Create a payment method if we use alias.
    if ($payment_method = $this->createPaymentMethod($transaction_data)) {
      $order->set('payment_method', $payment_method);
      $order->save();

      // If there was no initial amount, authorize another payment and
      // replace the transaction data information for setting the payment
      // info.
      // @todo The initial transaction is then no longer referenced.
      if ($this->configuration['no_initial_amount']) {
        $authorize_data = $this->authorizePayment($payment, $payment_method);
        $response = $this->doRequest('transactions/' . $authorize_data['transactionId'], [], 'GET');
        $transaction_data = Json::decode((string) $response->getBody());

        $payment->setRemoteId($transaction_data['transactionId']);
      }
    }

    // Only support certain stati, ignore others and do not create a payment.
    if (\in_array($transaction_data['status'], ['settled', 'transmitted'])) {
      $payment->setState('completed');
    }
    elseif (\in_array($transaction_data['status'], ['authorized'])) {
      $payment->setState('authorization');
    }
    else {
      throw new PaymentGatewayException($this->t('Payment @transaction_id failed with unexpected status @status.', [
        '@transaction_id' => $transaction_data['transactionId'],
        '@status' => $transaction_data['status'],
      ]));
    }

    $payment->setRemoteState($transaction_data['status']);
    if (!$payment->getAuthorizedTime()) {
      $payment->setAuthorizedTime($this->time->getRequestTime());
    }
    if ($transaction_data['status'] == 'settled') {
      $payment->setCompletedTime($this->time->getRequestTime());
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment->save();

    return $payment;
  }


  /**
   * Create an alias payment method.
   *
   * @todo https://www.drupal.org/node/2838380
   *
   * @param array $transaction_data
   *   Transaction status data.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentMethodInterface|null
   *   The created payment method.
   */
  public function createPaymentMethod(array $transaction_data) {
    // The alias can either be in a payment method specific element or in card.
    // If none can be found,
    if (isset($transaction_data[$transaction_data['paymentMethod']]['alias'])) {
      $remote_id = $transaction_data[$transaction_data['paymentMethod']]['alias'];
    }
    elseif (!empty($transaction_data['card']['alias'])) {
      $remote_id = $transaction_data['card']['alias'];
    }
    else {
      return NULL;
    }

    $payment_method = PaymentMethod::create([
      'payment_gateway' => $this->parentEntity->id(),
      'type' => 'datatrans_alias',
      'reusable' => TRUE,
      'pmethod' => $transaction_data['paymentMethod'],
      'masked_cc' => $transaction_data['card']['masked'] ?? NULL,
      'remote_id' => $remote_id,
    ]);

    if (!empty($transaction_data['card']['expiryMonth']) && !empty($transaction_data['card']['expiryYear'])) {
      // @todo remove custom fields?
      $payment_method->set('expm', $transaction_data['card']['expiryMonth']);
      $payment_method->set('expy', $transaction_data['card']['expiryYear']);

      $expires = CreditCard::calculateExpirationTimestamp($transaction_data['card']['expiryMonth'], $transaction_data['card']['expiryYear']);
      $payment_method->setExpiresTime($expires);
    }
    $payment_method->save();
    return $payment_method;
  }

  /**
   * Delete an alias payment method.
   *
   * @todo https://www.drupal.org/node/2838380
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the remote record here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    $data = [
      'amount' => $this->minorUnitsConverter->toMinorUnits($amount),
      'currency' => $amount->getCurrencyCode(),
      'refno' => $payment->getOrderId(),
    ];

    $url = 'transactions/' . $payment->getRemoteId() . '/credit';

    try {
      $response = $this->doRequest($url, $data);
    }
    catch (ClientException $e) {

      $response = $e->getResponse();
      $body = Json::decode((string) $response->getBody());

      $arguments = [
        '@code' => $body['error']['code'],
        '@error' => $body['error']['message'],
      ];
      throw new PaymentGatewayException($this->t('Refund failed with code @code: @error', $arguments), 0, $e);
    }
    catch (\Exception $e) {
      throw new PaymentGatewayException($this->t('Refund failed: @error.', ['@error' => $e->getMessage()]), 0, $e);
    }

    if ($response->getStatusCode() != 200) {
      throw new PaymentGatewayException($this->t('Refund failed with unexpected response: @response', ['@response' => (string) $response->getBody()]));
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->state = 'partially_refunded';
    }
    else {
      $payment->state = 'refunded';
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * Initializes a Payment based on the given payment and additional data.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment that must be paid.
   * @param array $data
   *   Any additional data to be sent with the request.
   *
   * @return \Drupal\commerce_datatrans\PaymentInitializeResponse
   *   The transaction information if the initialization was successful.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   A HTTP exception with the response from datatrans in case of an error.
   */
  public function initializePayment(PaymentInterface $payment, array $data = []): PaymentInitializeResponse {
    $order = $payment->getOrder();

    // Calculate the amount in the form Datatrans expects it.
    $amount = $this->minorUnitsConverter->toMinorUnits($payment->getAmount());

    $data = NestedArray::mergeDeep($data, [
      'currency' => $payment->getAmount()->getCurrencyCode(),
      'refno' => $order->id(),
      'autoSettle' => (bool) $this->configuration['auto_settle'],
      'amount' => (int) $amount,
    ]);

    // If use alias option was enabled in method configuration apply this for
    // this payment method plugin.
    if ($this->configuration['use_alias']) {
      $data['option']['createAlias'] = true;
      $data['option']['returnMaskedCardNumber'] = true;

      if ($this->configuration['no_initial_amount']) {
        $data['amount'] = 0;
      }
    }

    $this->moduleHandler->alter('commerce_datatrans_initialize_payment', $data, $payment, $this);

    try {
      $response = $this->doRequest('transactions', $data);
    }
    catch (ClientException $e) {

      $response = $e->getResponse();
      $body = Json::decode((string) $response->getBody());

      $arguments = [
        '@code' => $body['error']['code'],
        '@error' => $body['error']['message'],
      ];
      throw new PaymentGatewayException($this->t('Payment initialize failed with code @code: @error', $arguments), 0, $e);
    }
    catch (\Exception $e) {
      throw new PaymentGatewayException($this->t('Payment initialize failed: @error.', ['@error' => $e->getMessage()]), 0, $e);
    }

    $body = Json::decode((string) $response->getBody());

    return new PaymentInitializeResponse($response->getHeaderLine('Location'), $body);
  }

  /**
   * Authorizes a Payment based on the given payment and additional data.
   *
   * Requires a payment method for the alias information, does not require
   * manual interaction.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment that must be paid.
   * @param array $data
   *   Any additional data to be sent with the request.
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method with the alias information.
   *
   * @return array
   *   The transaction information if the authorization was successful.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   A HTTP exception with the response from datatrans in case of an error.
   */
  public function authorizePayment(PaymentInterface $payment, PaymentMethodInterface $payment_method, array $data = []): array {
    $order = $payment->getOrder();

    // Calculate the amount in the form Datatrans expects it.
    $amount = $this->minorUnitsConverter->toMinorUnits($payment->getAmount());

    $data = NestedArray::mergeDeep($data, [
      'currency' => $payment->getAmount()->getCurrencyCode(),
      'refno' => $order->id(),
      'autoSettle' => (bool) $this->configuration['auto_settle'],
      'amount' => (int) $amount,
    ]);

    // @todo are there more providers that use aliases like this?
    if (in_array($payment_method->get('pmethod')->value, ['TWI', 'PAP', 'PFC'])) {
      $data[$payment_method->get('pmethod')->value]['alias'] = $payment_method->getRemoteId();
    }
    else {
      $data['card']['alias'] = $payment_method->getRemoteId();
      $expire_date = DrupalDateTime::createFromTimestamp($payment_method->getExpiresTime());
      $data['card']['expiryMonth'] = $expire_date->format('m');
      $data['card']['expiryYear'] = $expire_date->format('y');
    }

    $this->moduleHandler->alter('commerce_datatrans_authorize_payment', $data, $payment, $this);

    try {
      $response = $this->doRequest('transactions/authorize', $data);
    }
    catch (ClientException $e) {

      $response = $e->getResponse();
      $body = Json::decode((string) $response->getBody());

      $arguments = [
        '@code' => $body['error']['code'],
        '@error' => $body['error']['message'],
      ];
      throw new PaymentGatewayException($this->t('Payment authorize failed with code @code: @error', $arguments), 0, $e);
    }
    catch (\Exception $e) {
      throw new PaymentGatewayException($this->t('Payment authorize failed: @error.', ['@error' => $e->getMessage()]), 0, $e);
    }

    return Json::decode((string) $response->getBody());
  }

  /**
   * Executes an API request and returns the response.
   *
   * @param string $url
   *   The API path without a leading slash.
   * @param array $data
   *   The data to be sent as JSON.
   * @param string $method
   *   The HTTP method, defaults to POST.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *    The API response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Thrown in case of an authentication error or invalid request.
   */
  protected function doRequest(string $url, array $data, string $method = 'POST'): ResponseInterface {
    if ($this->getMode() == 'test') {
      $hostname = 'api.sandbox.datatrans.com';
    }
    else {
      $hostname = 'api.datatrans.com';
    }
    $url = 'https://' . $hostname . '/v1/' . $url;

    $options = [
      'allow_redirects' => FALSE,
      'auth' => [
        $this->configuration['merchant_id'],
        $this->configuration['password']
      ],
      'json' => $data,
      'headers' => [
        'Content-Type' => 'application/json'
      ]
    ];

    return $this->httpClient->request($method, $url, $options);
  }

  /**
   * Loads the payment for the given transaction ID.
   *
   * @param int $transaction_id
   *   The datatrans transaction ID.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface|null
   *   The payment or NULL if none exists.
   */
  protected function getPaymentForTransactionId($transaction_id): ?PaymentInterface {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

    // If there is an existing payment that is not in pending state,
    // this transaction has already been processed, abort.
    $payments = $payment_storage->loadByProperties(
      [
        'remote_id' => $transaction_id,
        'payment_gateway' => $this->parentEntity->id(),
      ]
    );
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    return $payments ? reset($payments) : NULL;
  }

}
