<?php

namespace Drupal\commerce_datatrans\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Datatrans payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "datatrans",
 *   label = "Datatrans",
 *   display_label = "Datatrans",
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_datatrans\PluginForm\DatatransForm",
 *   },
 *   payment_method_types = {"credit_card", "datatrans_alias"},
 *   credit_card_types = {
 *     "VIS", "ECA", "AMX", "BPY", "DIN", "DIS", "DEA", "DIB", "DII", "DNK",
 *     "DVI", "ELV", "ESY", "JCB", "JEL", "MAU", "MDP", "MFA", "MFG", "MFX",
 *     "MMS", "MNB", "MYO", "PAP", "PEF", "PFC", "PSC", "PYL", "PYO", "REK",
 *     "SWB", "TWI", "MPW", "ACC", "INT", "PPA", "GPA", "GEP"
 *   },
 * )
 */
class Datatrans extends DatatransBase implements SupportsRefundsInterface, OffsitePaymentGatewayInterface {

  use LoggerChannelTrait;

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $transaction_id = $request->query->get('datatransTrxId');

    // Check if a payment already exists to avoid an extra API request, then
    // the webhook successfully processed this already.
    $payment = $this->getPaymentForTransactionId($transaction_id);
    if ($payment && $payment->getState()->value != 'pending') {
      return;
    }

    // Fall back to checking the payment status.
    try {
      $response = $this->doRequest('transactions/' . $transaction_id, [], 'GET');
      $transaction_data = Json::decode((string) $response->getBody());

      $this->processPayment($transaction_data, $order);
    }
    catch (PaymentGatewayException $e) {
      // Rethrow payment gateway exceptions unchanged.
      throw $e;
    }
    catch (\Exception $e) {
      Error::logException($this->getLogger('commerce_datatrans'), $e);
      throw new PaymentGatewayException($this->t('There was a problem while processing your payment.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getNotifyUrl() {
    return Url::fromRoute('commerce_datatrans.notify', [], ['absolute' => TRUE]);
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->messenger()->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
      '@gateway' => $this->getDisplayLabel(),
    ]));
  }

}
