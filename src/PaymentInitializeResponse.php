<?php

namespace Drupal\commerce_datatrans;

/**
 * Represents a response for a initialize payment request.
 */
class PaymentInitializeResponse {

  /**
   * The redirect location URL.
   *
   * @var string
   */
  protected $location;

  /**
   * The return response data.
   *
   * @var array
   */
  protected $response;

  /**
   * PaymentInitializeResponse constructor.
   *
   * @param string $location
   *   The redirect location URL.
   * @param array $response
   *   The return response data.
   */
  public function __construct(string $location, array $response) {
    $this->location = $location;
    $this->response = $response;
  }

  /**
   * Returns the redirect location URL.
   *
   * @return string
   *   The redirect location URL.
   */
  public function getLocation(): string {
    return $this->location;
  }

  /**
   * Returns the transaction ID.
   * @return int
   *   The transaction ID.
   */
  public function getTransactionId(): int {
    return $this->response['transactionId'];
  }

  /**
   * Returns the transaction ID.
   *
   * @return array
   *   The transaction ID.
   */
  public function getResponse(): array {
    return $this->response;
  }

}
