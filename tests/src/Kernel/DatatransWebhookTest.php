<?php

namespace Drupal\Tests\commerce_datatrans\Kernel;

use Drupal\commerce_datatrans\Controller\PaymentNotificationController;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @coversDefaultClass \Drupal\commerce_datatrans\Controller\PaymentNotificationController
 *
 * @group commerce_datatrans
 */
class DatatransWebhookTest extends DatatransKernelTestBase {

  /**
   * @covers ::notifyPage
   */
  public function testWebhookSuccess() {
    // Create the request and call the notify method.
    $request = Request::create('', 'POST', [], [], [], [], Json::encode($this->getTransactionData()));
    $request->headers->set('Datatrans-Signature', 't=12435676,s0=b25fc55b8472be2d603f22c3f8678f144a09a20c7c15be1753eefece9d58dbff');

    $controller = PaymentNotificationController::create($this->container);
    $controller->notifyPage($request);

    // Process a second time, ensure that no duplicate is created.
    $controller->notifyPage($request);

    $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadMultiple(NULL);
    $this->assertCount(1, $payments);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = reset($payments);

    $this->assertEquals('completed', $payment->getState()->value);
    $this->assertEquals(123, $payment->getRemoteId());
    $this->assertEquals($this->order->getTotalPrice(), $payment->getAmount());
    $this->assertEquals('datatrans_id', $payment->getPaymentGatewayId());
  }

  /**
   * @covers ::notifyPage
   */
  public function testWebhookNoData() {
    // Create the request and call the notify method.
    $request = Request::create('', 'POST', [], [], [], []);

    $this->expectException(NotFoundHttpException::class);

    $controller = PaymentNotificationController::create($this->container);
    $controller->notifyPage($request);
  }

  /**
   * @covers ::notifyPage
   */
  public function testWebhookNoSignature() {
    // Create the request and call the notify method.
    $request = Request::create('', 'POST', [], [], [], [], Json::encode($this->getTransactionData()));

    $this->expectException(AccessDeniedHttpException::class);

    $controller = PaymentNotificationController::create($this->container);
    $controller->notifyPage($request);
  }

  /**
   * @covers ::notifyPage
   */
  public function testWebhookWrongSignature() {
    // Create the request and call the notify method.
    $request = Request::create('', 'POST', [], [], [], [], Json::encode($this->getTransactionData()));
    $request->headers->set('Datatrans-Signature', 't=12435676,s0=wrong');

    $this->expectException(AccessDeniedHttpException::class);

    $controller = PaymentNotificationController::create($this->container);
    $controller->notifyPage($request);
  }

}
