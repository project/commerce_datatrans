<?php

namespace Drupal\Tests\commerce_datatrans\Kernel;

use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;

/**
 * Base class for Datatrans kernel tests.
 */
abstract class DatatransKernelTestBase extends CommerceKernelTestBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The payment gateway config entity.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   */
  protected $paymentGateway;

  protected static $modules = [
    'entity_reference_revisions',
    'state_machine',
    'profile',
    'commerce_number_pattern',
    'commerce_order',
    'commerce_payment',
    'commerce_datatrans',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installEntitySchema('commerce_payment');
    $this->installConfig(['commerce_order']);

    OrderItemType::create([
      'id' => 'default',
      'label' => 'Default',
      'orderType' => 'default',
    ])->save();

    $this->entityTypeManager = $this->container->get('entity_type.manager');

    // Create an order.
    $this->order = $this->entityTypeManager->getStorage('commerce_order')->create([
      'type' => 'default',
      'order_number' => '1',
      'store_id' => $this->store->id(),
      'state' => 'draft',
    ]);
    $this->order->save();

    $order_item = $this->entityTypeManager->getStorage('commerce_order_item')->create([
      'type' => 'default',
      'unit_price' => [
        'number' => '999',
        'currency_code' => 'USD',
      ],
    ]);
    $order_item->save();
    $this->order->setItems([$order_item]);
    $this->paymentGateway = PaymentGateway::create([
      'id' => 'datatrans_id',
      'plugin' => 'datatrans',
      'configuration' => [
        'password' => 'secret',
        'hmac_key_2' => '06c0d8316be07c3c7a701ab3e14c802f3925f0daa6855d38a4de19e58bccc0ef3868a50284586db0ca3822df7e89a2a02b528cd4d8e0a5221fecf9a94314b56d',
      ]
    ]);
    $this->paymentGateway->save();

    $this->order->set('payment_gateway', $this->paymentGateway->id());
    $this->order->save();
  }

  /**
   * Returns the datatrans gateway.
   *
   * @return \Drupal\commerce_datatrans\Plugin\Commerce\PaymentGateway\Datatrans
   *   The datatrans gateway.
   */
  protected function getDatatransGateway() {
    $configuration = [
      '_entity' => $this->paymentGateway,
    ] + $this->paymentGateway->getPluginConfiguration();
    $gateway_plugin_manager = $this->container->get('plugin.manager.commerce_payment_gateway');
    return $gateway_plugin_manager->createInstance('datatrans', $configuration);
  }

  /**
   * Returns example transaction data.
   *
   * @param string $status
   *   The payment status.
   *
   * @return array
   *   The data as returned by the Datatrans API.
   */
  protected function getTransactionData(string $status = 'settled'): array {
    return [
      'transactionId' => '123',
      'type' => 'payment',
      'status' => $status,
      'currency' => 'USD',
      'refno' => '1',
      'paymentMethod' => 'VIS',
      'detail' => [
        'authorize' => [
          'amount' => 999,
          'acquirerAuthorizationCode' => '180001',
        ],
        'settle' => [
          'amount' => 999,
        ],
      ],
      'card' => [
        'masked' => '400360xxxxxx0006',
        'expiryMonth' => '12',
        'expiryYear' => '21',
        'info' => [
          'brand' => 'VISA',
          'type' => 'credit',
          'usage' => 'corporate',
          'country' => 'CH',
          'issuer' => 'DATATRANS',
        ],
        '3D' => [
          'authenticationResponse' => 'D',
        ],
      ],
      'history' => [],
    ];
  }

}
