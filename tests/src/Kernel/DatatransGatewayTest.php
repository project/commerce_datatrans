<?php

namespace Drupal\Tests\commerce_datatrans\Kernel;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_price\Price;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * @coversDefaultClass \Drupal\commerce_datatrans\Plugin\Commerce\PaymentGateway\Datatrans
 *
 * @group commerce_datatrans
 */
class DatatransGatewayTest extends DatatransKernelTestBase {

  /**
   * @covers ::onReturn
   * @covers ::processPayment
   */
  public function testOnReturnExistingNewPayment() {

    $http_client = $this->prophesize(ClientInterface::class);
    $expected_options = [
      'allow_redirects' => FALSE,
      'auth' => ['', 'secret'],
      'json' => [],
      'headers' => ['Content-Type' => 'application/json'],
    ];

    $transaction_data = $this->getTransactionData();
    $response = new Response(200, [], Json::encode($transaction_data));

    $http_client->request('GET', 'https://api.sandbox.datatrans.com/v1/transactions/123', $expected_options)->willReturn($response);
    $this->container->set('http_client', $http_client->reveal());

    $datatrans = $this->getDatatransGateway();

    // Create an existing, pending payment.
    $payment = Payment::create([
      'payment_gateway' => $this->paymentGateway->id(),
      'remote_id' => 123,
      'order_id' => $this->order->id(),
      'state' => 'pending',
      'amount' => $this->order->getTotalPrice(),
    ]);
    $payment->save();

    // Create the request and call onReturn().
    $request = Request::create('', 'GET', ['datatransTrxId' => 123]);
    $datatrans->onReturn($this->order, $request);

    // Process a second time, ensure that no duplicate is created.
    $datatrans->onReturn($this->order, $request);

    $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadMultiple(NULL);
    $this->assertCount(1, $payments);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = reset($payments);

    $this->assertEquals('completed', $payment->getState()->value);
    $this->assertEquals(123, $payment->getRemoteId());
    $this->assertEquals($this->order->getTotalPrice(), $payment->getAmount());
    $this->assertEquals('datatrans_id', $payment->getPaymentGatewayId());
    $this->assertEquals(\Drupal::time()->getRequestTime(), $payment->getAuthorizedTime());
    $this->assertEquals(\Drupal::time()->getRequestTime(), $payment->getCompletedTime());

  }

  /**
   * @covers ::onReturn
   * @covers ::processPayment
   */
  public function testOnReturnSettled() {

    $http_client = $this->prophesize(ClientInterface::class);
    $expected_options = [
      'allow_redirects' => FALSE,
      'auth' => ['', 'secret'],
      'json' => [],
      'headers' => ['Content-Type' => 'application/json'],
    ];

    $transaction_data = $this->getTransactionData();
    $response = new Response(200, [], Json::encode($transaction_data));

    $http_client->request('GET', 'https://api.sandbox.datatrans.com/v1/transactions/123', $expected_options)->willReturn($response);
    $this->container->set('http_client', $http_client->reveal());

    $datatrans = $this->getDatatransGateway();

    // Create the request and call onReturn().
    $request = Request::create('', 'GET', ['datatransTrxId' => 123]);
    $datatrans->onReturn($this->order, $request);

    // Process a second time, ensure that no duplicate is created.
    $datatrans->onReturn($this->order, $request);

    $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadMultiple(NULL);
    $this->assertCount(1, $payments);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = reset($payments);

    $this->assertEquals('completed', $payment->getState()->value);
    $this->assertEquals(123, $payment->getRemoteId());
    $this->assertEquals($this->order->getTotalPrice(), $payment->getAmount());
    $this->assertEquals('datatrans_id', $payment->getPaymentGatewayId());
    $this->assertEquals(\Drupal::time()->getRequestTime(), $payment->getAuthorizedTime());
    $this->assertEquals(\Drupal::time()->getRequestTime(), $payment->getCompletedTime());
  }

  /**
   * @covers ::onReturn
   * @covers ::processPayment
   */
  public function testOnReturnTransmitted() {

    $http_client = $this->prophesize(ClientInterface::class);
    $expected_options = [
      'allow_redirects' => FALSE,
      'auth' => ['', 'secret'],
      'json' => [],
      'headers' => ['Content-Type' => 'application/json'],
    ];

    $transaction_data = $this->getTransactionData('transmitted');
    $response = new Response(200, [], Json::encode($transaction_data));

    $http_client->request('GET', 'https://api.sandbox.datatrans.com/v1/transactions/123', $expected_options)->willReturn($response);
    $this->container->set('http_client', $http_client->reveal());

    $datatrans = $this->getDatatransGateway();

    // Create the request and call onReturn().
    $request = Request::create('', 'GET', ['datatransTrxId' => 123]);
    $datatrans->onReturn($this->order, $request);

    // Process a second time, ensure that no duplicate is created.
    $datatrans->onReturn($this->order, $request);

    $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadMultiple(NULL);
    $this->assertCount(1, $payments);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = reset($payments);

    $this->assertEquals('completed', $payment->getState()->value);
    $this->assertEquals(123, $payment->getRemoteId());
    $this->assertEquals($this->order->getTotalPrice(), $payment->getAmount());
    $this->assertEquals('datatrans_id', $payment->getPaymentGatewayId());
    $this->assertEquals(\Drupal::time()->getRequestTime(), $payment->getAuthorizedTime());
    $this->assertEquals(\Drupal::time()->getRequestTime(), $payment->getCompletedTime());
  }

  /**
   * @covers ::onReturn
   * @covers ::processPayment
   */
  public function testOnReturnAuthorizeOnly() {
    $http_client = $this->prophesize(ClientInterface::class);
    $expected_options = [
      'allow_redirects' => FALSE,
      'auth' => ['', 'secret'],
      'json' => [],
      'headers' => ['Content-Type' => 'application/json'],
    ];

    $response = new Response(200, [], Json::encode($this->getTransactionData('authorized')));

    $http_client->request('GET', 'https://api.sandbox.datatrans.com/v1/transactions/123', $expected_options)->willReturn($response);
    $this->container->set('http_client', $http_client->reveal());

    // Initialize the datatrans gateway.
    $datatrans = $this->getDatatransGateway();

    // Create the request and call onReturn().
    $request = Request::create('', 'GET', ['datatransTrxId' => 123]);
    $datatrans->onReturn($this->order, $request);

    $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadMultiple(NULL);
    $this->assertCount(1, $payments);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = reset($payments);

    $this->assertEquals('authorization', $payment->getState()->value);
    $this->assertEquals(123, $payment->getRemoteId());
    $this->assertEquals($this->order->getTotalPrice(), $payment->getAmount());
    $this->assertEquals('datatrans_id', $payment->getPaymentGatewayId());
    $this->assertEquals(\Drupal::time()->getRequestTime(), $payment->getAuthorizedTime());
    $this->assertNull($payment->getCompletedTime());
  }

  /**
   * @covers ::onReturn
   * @covers ::processPayment
   * @covers ::authorizePayment
   * @covers ::createPaymentMethod
   */
  public function testOnReturnCardAliasNoInitialAmount() {

    $this->installEntitySchema('commerce_payment_method');

    $configuration = $this->paymentGateway->getPluginConfiguration();
    $configuration['use_alias'] = TRUE;
    $configuration['no_initial_amount'] = TRUE;
    $this->paymentGateway->setPluginConfiguration($configuration);
    $this->paymentGateway->save();

    // This requires 3 API requests. fetching the initial translation,
    // authorizing the follow-up transaction and fetching that transaction.
    $http_client = $this->prophesize(ClientInterface::class);
    $expected_options = [
      'allow_redirects' => FALSE,
      'auth' => ['', 'secret'],
      'json' => [],
      'headers' => ['Content-Type' => 'application/json'],
    ];

    $transaction_data = $this->getTransactionData('authorized');
    $transaction_data['card']['alias'] = 'SuperSecretAlias';

    $response = new Response(200, [], Json::encode($transaction_data));
    $http_client->request('GET', 'https://api.sandbox.datatrans.com/v1/transactions/123', $expected_options)->willReturn($response);

    $authorize_expected_options = [
      'allow_redirects' => FALSE,
      'auth' => ['', 'secret'],
      'json' => [
        'currency' => 'USD',
        'refno' => '1',
        'amount' => 99900,
        'autoSettle' => true,
        'card' => [
          'alias' => 'SuperSecretAlias',
          'expiryMonth' => '12',
          'expiryYear' => '21'
        ]
      ],
      'headers' => ['Content-Type' => 'application/json'],
    ];
    $response = new Response(200, [], Json::encode(['transactionId' => 456]));
    $http_client->request('POST', 'https://api.sandbox.datatrans.com/v1/transactions/authorize', $authorize_expected_options)->willReturn($response);

    $transaction_data = $this->getTransactionData();
    $transaction_data['transactionId'] = 456;

    $response = new Response(200, [], Json::encode($transaction_data));
    $http_client->request('GET', 'https://api.sandbox.datatrans.com/v1/transactions/456', $expected_options)->willReturn($response);

    $this->container->set('http_client', $http_client->reveal());

    // Initialize the datatrans gateway.
    $datatrans = $this->getDatatransGateway();

    // Create the request and call onReturn().
    $request = Request::create('', 'GET', ['datatransTrxId' => 123]);
    $datatrans->onReturn($this->order, $request);

    $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadMultiple(NULL);
    $this->assertCount(1, $payments);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = reset($payments);

    $this->assertEquals('completed', $payment->getState()->value);
    $this->assertEquals(456, $payment->getRemoteId());
    $this->assertEquals($this->order->getTotalPrice(), $payment->getAmount());
    $this->assertEquals('datatrans_id', $payment->getPaymentGatewayId());
    $this->assertEquals(\Drupal::time()->getRequestTime(), $payment->getAuthorizedTime());
    $this->assertEquals(\Drupal::time()->getRequestTime(), $payment->getCompletedTime());

    // Check the payment method.
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = Order::load($this->order->id())->get('payment_method')->entity;
    $this->assertEquals('SuperSecretAlias', $payment_method->getRemoteId());
    $this->assertEquals('1640955599', $payment_method->getExpiresTime());
    $this->assertEquals($this->paymentGateway->id(), $payment_method->getPaymentGatewayId());
    $this->assertEquals('VIS', $payment_method->get('pmethod')->value);
    $this->assertEquals('400360xxxxxx0006', $payment_method->get('masked_cc')->value);
    $this->assertEquals('datatrans_alias', $payment_method->bundle());
  }

  /**
   * @covers ::onReturn
   * @covers ::processPayment
   */
  public function testOnReturnUnknownState() {
    $http_client = $this->prophesize(ClientInterface::class);
    $expected_options = [
      'allow_redirects' => FALSE,
      'auth' => ['', 'secret'],
      'json' => [],
      'headers' => ['Content-Type' => 'application/json'],
    ];

    $response = new Response(200, [], Json::encode($this->getTransactionData('failed')));

    $http_client->request('GET', 'https://api.sandbox.datatrans.com/v1/transactions/123', $expected_options)->willReturn($response);
    $this->container->set('http_client', $http_client->reveal());

    // Initialize the datatrans gateway.
    $datatrans = $this->getDatatransGateway();

    $this->expectException(PaymentGatewayException::class);
    $this->expectExceptionMessage('Payment 123 failed with unexpected status failed.');

    // Create the request and call onReturn().
    $request = Request::create('', 'GET', ['datatransTrxId' => 123]);
    $datatrans->onReturn($this->order, $request);
  }

  /**
   * @covers ::refundPayment
   */
  public function testRefundPayment() {

    $http_client = $this->prophesize(ClientInterface::class);
    $expected_options = [
      'allow_redirects' => FALSE,
      'auth' => ['', 'secret'],
      'json' => ['amount' => 99900, 'currency' => 'USD', 'refno' => '1'],
      'headers' => ['Content-Type' => 'application/json'],
    ];
    $http_client->request('POST', 'https://api.sandbox.datatrans.com/v1/transactions/123/credit', $expected_options)->willReturn(new Response());
    $this->container->set('http_client', $http_client->reveal());

    $datatrans = $this->getDatatransGateway();

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = Payment::create([
      'state' => 'completed',
      'amount' => $this->order->getTotalPrice(),
      'payment_gateway' => $this->paymentGateway->id(),
      'order_id' => $this->order->id(),
      'test' => TRUE,
      'remote_id' => '123',
      'remote_state' => 'test',
      'authorized' => \Drupal::time()->getRequestTime(),
    ]);
    $payment->save();

    $datatrans->refundPayment($payment);

    $this->assertEquals('refunded', $payment->getState()->value);
    $this->assertEquals(123, $payment->getRemoteId());
    $this->assertEquals($this->order->getTotalPrice(), $payment->getAmount());
    $this->assertEquals($this->order->getTotalPrice(), $payment->getRefundedAmount());
    $this->assertEquals(new Price(0, 'USD'), $payment->getBalance());
    $this->assertEquals('datatrans_id', $payment->getPaymentGatewayId());
  }

  /**
   * @covers ::refundPayment
   */
  public function testRefundPaymentPartial() {

    $http_client = $this->prophesize(ClientInterface::class);
    $expected_options = [
      'allow_redirects' => FALSE,
      'auth' => ['', 'secret'],
      'json' => ['amount' => 12350, 'currency' => 'USD', 'refno' => '1'],
      'headers' => ['Content-Type' => 'application/json'],
    ];
    $http_client->request('POST', 'https://api.sandbox.datatrans.com/v1/transactions/123/credit', $expected_options)->willReturn(new Response());
    $this->container->set('http_client', $http_client->reveal());

    $datatrans = $this->getDatatransGateway();

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = Payment::create([
      'state' => 'completed',
      'amount' => $this->order->getTotalPrice(),
      'payment_gateway' => $this->paymentGateway->id(),
      'order_id' => $this->order->id(),
      'test' => TRUE,
      'remote_id' => '123',
      'remote_state' => 'test',
      'authorized' => \Drupal::time()->getRequestTime(),
    ]);
    $payment->save();

    $refund_amount = new Price('123.50', 'USD');
    $datatrans->refundPayment($payment, $refund_amount);

    $this->assertEquals('partially_refunded', $payment->getState()->value);
    $this->assertEquals(123, $payment->getRemoteId());
    $this->assertEquals($this->order->getTotalPrice(), $payment->getAmount());
    $this->assertEquals($refund_amount, $payment->getRefundedAmount());
    $this->assertEquals(new Price('875.5', 'USD'), $payment->getBalance());
    $this->assertEquals('datatrans_id', $payment->getPaymentGatewayId());
  }

}
